## Zadání úlohy

Ve 2D rovinně máme kruhové zrcadlo, počáteční bod a koncový bod.
Paprsek laseru svítí z počátečního bodu ve směru koncového bodu.
Spočtěte a vykreslete kde všude se paprsek odrazí a kudy prochází.

![Náčrt výpočtu odrazu](./howto.png)

Výsledek zobecněte pro více zrcadel.

![Visualisace řešení](./img.png)


## Plán přednášky

### Teorie

* zadání úlohy
* reprezentace bodů (vektorů), přímky, kruhu
* reprezentace vstupu a výstupu úlohy
* vzdálenost bodů, velikost vektoru, výpočet délky odvěsny (cathethus / leg)
* skalární (dot) a vektorový součin (cross), jejich význam

### Kódění

"Nehotový" kód je v souboru TodoLaser.

* co bude dělat metoda findReflectionPoint()
    * co jsou vstupy (Line laser, Circle circle)
    * co bude výstupem (Line reflected\_line)
    * co znamená, když vrátí null?
* implementovat draw() bez hotové findReflection
    * resetovat obě úsečky
    * když vrátí null tak vykreslit pouze první k myši
    * když vrátí line, tak vykreslit první k bodu a druhou od bodu
* postupně kreslit obrázek metody a implementovat operace
    * vzdálenost přímky od centra kruhu
    * najít oba průsečíky pomocí dopočtu odvěsen trojúhelníku
    * zjistit, který průsečík je blíže a vybrat ho jako odrazový
    * spočíst úhel, pod kterým paprsek padá na kruh
    * spočíst úhel, o který otáčíme
    * otočit vektor a vrátit bod dopadu s tímto vektorem jako výsledek

