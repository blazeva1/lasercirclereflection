import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import acm.graphics.*;
import acm.program.*;
import sun.java2d.loops.DrawLine;

public class TodoLaser extends GraphicsProgram {

	GRect background; // background rectangle is used to catch mouse events
	int background_size = 600; // size of background rectangle
	Circle circle = new Circle(); // a mirroring circle
	Point start = new Point(100, 100); // starting point of the laser ray
	Line starting_line = new Line(); // laser from the initial position to reflection point
	Line reflected_line = new Line(); // reflected line segment of the laser ray

	// set mouse event to initiate recalculation of drawn lines
	@Override
	public void mousePressed(MouseEvent e) {
		draw(new Point(e.getX(), e.getY()));
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		draw(new Point(e.getX(), e.getY()));
	}
	
	// this is run only once, to initialize all the variables
	public void run() {
		background = new GRect(background_size, background_size);
		background.setColor(Color.BLACK);
		background.setFilled(true);
		background.setFillColor(Color.WHITE);
		background.addMouseMotionListener(this);
		background.addMouseListener(this);
		add(background, 0, 0);
		reflected_line = new Line();
		reflected_line.addToDrawing(this);
		starting_line = new Line();
		starting_line.addToDrawing(this);
		circle = new Circle(new Point(300, 150), 60);
		circle.addToDrawing(this);
		draw(new Point(120, 120));
	}
	
	// recalculate the laser ray 
	public void draw(Point mouse_position) {
		reflected_line.a = reflected_line.b = new Point(0, 0);
		
		// TODO (use findReflectionPoint method)
		
		circle.draw();
		starting_line.draw();
		reflected_line.draw();
		repaint();
	}

	
	// return Line with reflection point A and point where the reflected laser will shine through B
	private Line findReflectionPoint(Line laser, Circle circle) {
		// TODO
		return null;
	}
	
}
