import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import acm.graphics.*;
import acm.program.*;
import sun.java2d.loops.DrawLine;

public class LaserCircleOne extends GraphicsProgram {

	GRect background; // background rectangle is used to catch mouse events
	int background_size = 600; // size of background rectangle
	Circle circle = new Circle(); // a mirroring circle
	Line starting_line = new Line(); // laser from the initial position to reflection point
	Line line = new Line(); // reflected line segment of the laser ray
	Point start = new Point(100, 100); // starting point of the laser ray

	// set mouse event to initiate recalculation of drawn lines
	@Override
	public void mousePressed(MouseEvent e) {
		draw(new Point(e.getX(), e.getY()));
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		draw(new Point(e.getX(), e.getY()));
	}
	
	// this is run only once, to initialize all the variables
	public void run() {
		background = new GRect(background_size, background_size);
		background.setColor(Color.BLACK);
		background.setFilled(true);
		background.setFillColor(Color.WHITE);
		background.addMouseMotionListener(this);
		background.addMouseListener(this);
		add(background, 0, 0);
		line = new Line();
		line.addToDrawing(this);
		starting_line = new Line();
		starting_line.addToDrawing(this);
		circle = new Circle(new Point(300, 150), 60);
		circle.addToDrawing(this);
		draw(new Point(120, 120));
	}
	
	// recalculate the laser ray 
	public void draw(Point mouse_position) {
		line.a = line.b = new Point(0, 0);
		Line laser = new Line(start, mouse_position);
		starting_line.a = laser.a;
		starting_line.b = laser.b;
		Line reflected = findReflectionPoint(laser, circle);
		if(reflected != null) {
			starting_line.b = reflected.a;
			line.a = reflected.a;
			line.b = reflected.b;
		}
		circle.draw();
		starting_line.draw();
		line.draw();
		repaint();
	}

	
	// return Line with reflection point A and point where the reflected laser will shine through B
	private Line findReflectionPoint(Line laser, Circle circle) {
		Point start = laser.a;
		Point target = laser.b;
		Point direction = target.subtract(start);
		double center_line_distance = circle.center.distanceTo(laser);
		
		boolean okDirection = direction.dot(circle.center.subtract(start)) > 0;
		boolean lineCrossesCircle = Math.abs(center_line_distance) < circle.radius;
		if(okDirection && lineCrossesCircle) {
			double cathethus_length = Math.sqrt(Math.pow(circle.radius, 2) - Math.pow(center_line_distance, 2));
			Point scaled_direction = direction.scaleTo(cathethus_length);
			Point X = circle.center.add(direction.rotateBy(Math.PI/2).scaleTo(center_line_distance));
			Point A = X.add(scaled_direction);
			Point B = X.subtract(scaled_direction);
			Point reflection_point = start.distanceTo(A) < start.distanceTo(B) ? A : B;
			double dir = reflection_point.subtract(circle.center).cross(direction) > 0 ? 1 : -1;
			double mirror_angle = circle.center.subtract(reflection_point).getAngle(direction);
			double rotation_angle = dir * (Math.PI - 2 * Math.abs(mirror_angle));
			Point new_direction = direction.rotateBy(rotation_angle);
			return new Line(reflection_point, reflection_point.add(new_direction));
		} else {
			return null;
		}
	}
	
}
