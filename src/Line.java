import java.awt.Color;
import java.awt.event.MouseEvent;

import acm.graphics.*;
import acm.program.*;

public class Line {
	public Point a;
	public Point b;
	
	public Line(Point a, Point b) {
		this.a = a;
		this.b = b;
	}
	
	public Line() {
		this(new Point(), new Point());
	}
	
	public GLine drawing;
	
	public void addToDrawing(GraphicsProgram graphics) {
		this.drawing = new GLine(0,0,0,0);
		graphics.add(drawing);
	}
	
	public void draw() {
		drawing.setLocation(a.x, a.y);
		drawing.setEndPoint(b.x, b.y);
	}
}
