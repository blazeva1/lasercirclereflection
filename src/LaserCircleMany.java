import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import acm.graphics.*;
import acm.program.*;
import sun.java2d.loops.DrawLine;

public class LaserCircleMany extends GraphicsProgram {

	GRect background; // background rectangle is used to catch mouse events
	int background_size = 600; // size of background rectangle
	ArrayList<Circle> circles = new ArrayList<Circle>(); // all mirroring circles
	ArrayList<Line> lines = new ArrayList<Line>(); // line segments of the laser ray
	Point start = new Point(100, 100); // starting point of the laser ray
	Point end = new Point(120, 120); // starting point of the laser ray
	int number_of_lines = 100; // maximum number of lines

	// utility method for adding a circle which will be drawn in the program
	public void addCircle(Point center, double radius) {
		Circle circle = new Circle(center, radius);
		circles.add(circle);
		circle.addToDrawing(this);
	}

	// set mouse event to initiate recalculation of drawn lines
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == 1) end = new Point(e.getX(), e.getY());
		if(e.getButton() == 3) start = new Point(e.getX(), e.getY());
		draw();
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		end = new Point(e.getX(), e.getY());
		draw();
	}
	
	// this is run only once, to initialize all the variables
	public void run() {
		background = new GRect(background_size, background_size);
		background.setColor(Color.BLACK);
		background.setFilled(true);
		background.setFillColor(Color.WHITE);
		background.addMouseMotionListener(this);
		background.addMouseListener(this);
		add(background, 0, 0);
		for(int i=0; i<number_of_lines; ++i) {
			Line line = new Line();
			line.addToDrawing(this);
			lines.add(line);
		}
		int size = 18;
		addCircle(new Point(100, 200), size);
		addCircle(new Point(100, 240), size);
		addCircle(new Point(100, 280), size);
		addCircle(new Point(100, 320), size);
		addCircle(new Point(100, 360), size);
		addCircle(new Point(140, 200), size);
		addCircle(new Point(140, 280), size);
		addCircle(new Point(180, 280), size);
		addCircle(new Point(180, 200), size);

		addCircle(new Point(250, 200), size);
		addCircle(new Point(250, 240), size);
		addCircle(new Point(250, 280), size);
		addCircle(new Point(250, 320), size);
		addCircle(new Point(250, 360), size);
		
		addCircle(new Point(320, 200), size);
		addCircle(new Point(320, 240), size);
		addCircle(new Point(320, 280), size);
		addCircle(new Point(320, 320), size);
		addCircle(new Point(320, 360), size);
		addCircle(new Point(360, 280), size);
		addCircle(new Point(380, 240), size);
		addCircle(new Point(380, 320), size);
		addCircle(new Point(400, 200), size);
		addCircle(new Point(400, 360), size);

		addCircle(new Point(460, 220), size);
		addCircle(new Point(460, 320), size);
		addCircle(new Point(500, 200), size);
		addCircle(new Point(500, 280), size);
		addCircle(new Point(500, 360), size);
		addCircle(new Point(540, 240), size);
		addCircle(new Point(540, 340), size);
		
		draw();
	}
	
	// recalculate the laser ray 
	public void draw() {
		for(Line line : lines) line.a = line.b = new Point(0, 0);
		Line laser = new Line(start, end);
		lines.get(0).a = laser.a;
		lines.get(0).b = laser.b;
		for(int i=1; i<number_of_lines && laser != null; ++i) {
			Line closestReflection = null;
			for(Circle c : circles) {
				Line reflected = findReflectionPoint(laser, c);
				if(reflected != null) {
					if(closestReflection == null) {
						closestReflection = reflected;
					} else {
						if(laser.a.distanceTo(reflected.a) < laser.a.distanceTo(closestReflection.a)) {
							closestReflection = reflected;
						}
					}
				}
			}
			if(closestReflection != null) {
				lines.get(i-1).b = closestReflection.a;
				lines.get(i).a = closestReflection.a;
				lines.get(i).b = closestReflection.b;
			}
			laser = closestReflection;
		}
		for(Circle circle : circles) circle.draw();
		for(Line line : lines) line.draw();
		repaint();
	}

	
	// return Line with reflection point A and point where the reflected laser will shine through B
	private Line findReflectionPoint(Line laser, Circle circle) {
		Point start = laser.a;
		Point target = laser.b;
		Point direction = target.subtract(start);
		double center_line_distance = circle.center.distanceTo(laser);
		
		boolean okDirection = direction.dot(circle.center.subtract(start)) > 0;
		boolean lineCrossesCircle = Math.abs(center_line_distance) < circle.radius;
		if(okDirection && lineCrossesCircle) {
			double cathethus_length = Math.sqrt(Math.pow(circle.radius, 2) - Math.pow(center_line_distance, 2));
			Point scaled_direction = direction.scaleTo(cathethus_length);
			Point X = circle.center.add(direction.rotateBy(Math.PI/2).scaleTo(center_line_distance));
			Point A = X.add(scaled_direction);
			Point B = X.subtract(scaled_direction);
			Point reflection_point = start.distanceTo(A) < start.distanceTo(B) ? A : B;
			double dir = reflection_point.subtract(circle.center).cross(direction) > 0 ? 1 : -1;
			double mirror_angle = circle.center.subtract(reflection_point).getAngle(direction);
			double rotation_angle = dir * (Math.PI - 2 * Math.abs(mirror_angle));
			Point new_direction = direction.rotateBy(rotation_angle);
			return new Line(reflection_point, reflection_point.add(new_direction));
		} else {
			return null;
		}
	}
	
}
