import java.awt.Color;
import java.awt.event.MouseEvent;

import acm.graphics.*;
import acm.program.*;

public class Point {
	public double x;
	public double y;
	public double radius = 5;
	
	public Point(double x, double y){
		this.x = x;
		this.y = y;
		this.drawing = new GOval(0,0,radius,radius);
		drawing.setFillColor(Color.LIGHT_GRAY);
		drawing.setFilled(true);
	}
	
	public Point() {
		this(0, 0);
	}
	
	public Point add(Point a) {
		return new Point(x + a.x, y + a.y);
	}
	
	public Point subtract(Point a) {
		return new Point(x - a.x, y - a.y);
	}
	
	public Point scaleTo(double length) {
		return this.multiply(length / this.size());
	}

	public Point multiply(double a) {
		return new Point(x * a, y * a);
	}
	
	public double size() {
		return Math.sqrt(x * x + y * y);
	}
	
	public Point rotateBy(double angle) {
		double rx = x * Math.cos(angle) + y * Math.sin(angle);
		double ry = y * Math.cos(angle) - x * Math.sin(angle);
		return new Point(rx, ry);
	}
	
	public double distanceTo(Point a) {
		return this.subtract(a).size();
	}
	
	public double distanceTo(Line l) {
		Point direction = l.b.subtract(l.a);
		Point to_center = this.subtract(l.a);
		return direction.cross(to_center) / l.a.distanceTo(l.b);
	}
	
	public double cross(Point a) {
		return x * a.y - y * a.x;
	}
	
	public double dot(Point a) {
		return x * a.x + y * a.y;
	}

	// law of cosines
	public double getAngle(Point a) {
		return Math.acos(dot(a) / (size() * a.size()));
	}
	
	public GOval drawing;
	
	public void addToDrawing(GraphicsProgram graphics) {
		this.drawing = new GOval(0,0,0,0);
		graphics.add(drawing);
	}
	
	public void draw() {
		drawing.setLocation(x - radius, y - radius);
		drawing.setSize(2*radius, 2*radius);
	}
}
