import java.awt.Color;
import java.awt.event.MouseEvent;

import acm.graphics.*;
import acm.program.*;

public class Circle {
	public Point center;
	public double radius;
	
	public Circle(Point center, double radius){
		this.center = center;
		this.radius = radius;
	}
	
	public Circle() {
		this(new Point(0, 0), 0);
	}
	
	public GOval drawing;
	
	public void addToDrawing(GraphicsProgram graphics) {
		this.drawing = new GOval(0,0,0,0);
		this.drawing.setFilled(true);
		this.drawing.setFillColor(Color.LIGHT_GRAY);
		graphics.add(drawing);
	}
	
	public void draw() {
		drawing.setLocation(center.x - radius, center.y - radius);
		drawing.setSize(2*radius, 2*radius);
	}
}
